# Utilização de Banco de Dados SQLite no Android

## Criação de Projeto
Este projeto tem por objetivo guiá-lo na implementação de persistência de dados utilizando banco de dados `SQLite` em uma aplicação Android; para maiores informações sobre o SQLite acesse este [link](https://www.sqlite.org/index.html), para informações para Android neste [link](https://developer.android.com/training/data-storage/sqlite#java).
Desta vez iremos criar um projeto que utilize componentes do `Material Design`,
mais informações sobre os componentes [aqui](https://material.io).
1. Crie um novo projeto identificando-o como `PersistenciaDeDados`.
2. Escolha a Activity no tipo `Basic Activity`. Durante a aula iremos revisar os componentes deste tipo de Activity.

## Criação da classe Helper para manipulação de dados
Como discutido em sala, existe uma API no Android desde a primeira versão, esta API
facilita o gerenciamento de operações em bases de dados SQLite.
O Componente principal desta API é a classe `SQLiteOpenHelper`. Ela possui os métodos mais utilizados
para o gerenciamento das transações em banco.

1. Crie um novo pacote dentro do pacote principal chamado `helper`.
2. Crie uma nova classe e identifique-a por `MySQLiteOpenHelper` dentro do pacote helper.
3. Agora vamos 'estender'  a a classe SQLiteOpenHelper

```javascript
public class MySQLiteOpenHelper extends SQLiteOpenHelper
```  
4. Com a ajuda da IDE vamos corrigir os erros apontados com `Alt+Enter`, implementando os métodos obrigatórios.
![](images/img01.png)
![](images/img02.png)
5. Ainda temos trabalho a fazer, a IDE ainda aponta alguns erros, então `Alt+Enter` para resolver os conflitos criando o construtor de classe padrão.
```javascript
public class MyOpenSQLiteHelper extends SQLiteOpenHelper {

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
```
![](images/img03.png)
6. Nossa classe até este momento

```javascript
package com.esdras.databasecesario;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class MyOpenSQLiteHelper extends SQLiteOpenHelper {

    public MyOpenSQLiteHelper(@Nullable Context context,
                              @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
```
7. Vamos criar as constantes que irão nos ajudar a estruturar a query de criação do banco e da tabela.

```javascript
public class MyOpenSQLiteHelper extends SQLiteOpenHelper {

    private static final String BANCO = "dicionario.db";
    private static final String TABELA = "dicionario";
    private static final String CAMPO_PALAVRA = "palavra";
    private static final String CAMPO_DEFINICAO = "definicao";
    private static final int VERSAO = 1;
    //...restante do código
  }
```
8. Atualize o construtor da classe
```javascript
public MyOpenSQLiteHelper(@Nullable Context context) {
    super(context, BANCO, null, VERSAO);
}
```
9. Complete o método responsável pela criação da tabela na base de dados SQLite.
```javascript
@Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE "+ TABELA
                +"(_id integer primary key, "
                + CAMPO_PALAVRA +" TEXT,"
                + CAMPO_DEFINICAO +" TEXT);");
    }
```
10. Complete o método responsável pela evolução da tabela na base de dados SQLite.
```javascript
@Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DELETE FROM "+TABELA);
        onCreate(sqLiteDatabase);
    }
```
### Lembrete
Como discutimos em sala, os métodos auxiliares do `SQLiteOpenHelper` possuem uma forma peculiar de passagem de parametros de estruturacao de queries; cada operação possui uma parametrização específica conforme o quadro abaixo:

OPERAÇÃO | PARAMETRIZAÇÃO
-- | --
**INSERT** | public long insert (**String table, String nullColumnHack, ContentValues values**)
**READ** | public Cursor query (**String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy**)
**READ CLASSICO** | public Cursor rawQuery(**String sql, String[] selectionArgs**)
**DELETE** | public int delete (**String table, String whereClause, String[] whereArgs**)
**UPDATE** | public int update (**String table, ContentValues values, String whereClause, String[] whereArgs**)

11. Com o código do banco de dados e da tabela já criados, vamos partir para a implementação do `C` do nosso `CRUD` SQLite, ou seja o método de inserção ou criação de um registro na tabela. Inclua a função abaixo.
```javascript
public long inserirRegistro(String palavra, String definicao) {
    SQLiteDatabase sqLiteDatabase = getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(CAMPO_PALAVRA, palavra);
    values.put(CAMPO_DEFINICAO, definicao);
    return sqLiteDatabase.insert(TABELA, null, values);
}
```
12. Agora insira o código da função de implementação do `R` do `CRUD`; a leitura de um registro na base de dados.
```javascript
public String buscarDefinicaoPalavra(long id) {
        String resultado = "";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT definicao FROM "
                + TABELA + " WHERE _id = ?",
                new String[]{String.valueOf(id)});
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            resultado = cursor.getString(0);
        }
        return resultado;
    }
```
13. Agora insira a função do `U` do `CRUD`; a atualização de um registro na tabela.
```javascript
public int atualizarRegistro(long id, String word, String definition) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("_id", id);
        values.put(CAMPO_PALAVRA, word);
        values.put(CAMPO_DEFINICAO, definition);
        return db.update(TABELA, values, "_id = ?",
                new String[]{String.valueOf(id)});
}
```
14. Enfim, o `D`, o **DELETE** para remoção de um registro na tabela.
```javascript
public int apagarRegistro(long id) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.delete(TABELA, "_id = ?",
                new String[]{String.valueOf(id)});
}
```
15. Mais um `R`, vamos criar um método auxiliar para criação do método **salvar** mais estruturado. Este método irá buscar uma palavra na base de dados e retornar o seu id para suporte em outras buscas baseadas no id. Insira o método abaixo.
```javascript
public long buscarIdPalavra(String palavra) {
    long resultado = -1;
    SQLiteDatabase sqLiteDatabase = getReadableDatabase();
    Cursor cursor = sqLiteDatabase.rawQuery("SELECT _id FROM " + TABELA
            + " WHERE " + CAMPO_PALAVRA + " = ?", new String[]{palavra});
    Log.i("buscarIdPalavra","getCount()="+cursor.getCount());
    if (cursor.getCount() == 1) {
        cursor.moveToFirst();
        resultado = cursor.getInt(0);
    }
    return resultado;
}
```
16. Vamos criar um método que salvará um registro em banco, porém antes ele realiza uma busca para verificar se a a palavra a ser salva já existe na base, caso não exista, será criada, caso já exista na base, será atualizada. Utilizaremos o método `buscarIdPalavra` para nos auxiliar. Insira o código abaixo.
```javascript
public void salvarRegistro(String palavra, String definition){
    long id = buscarIdPalavra(palavra);
    if (id>0) {
        atualizarRegistro(id, palavra,definition);
    } else {
        inserirRegistro(palavra,definition);
    }
}
```

17. Vamos criar um método que retorna todos os registros do banco. Insira o código abaixo.
```javascript
public Cursor buscarTodosRegistros() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = "SELECT _id, " + CAMPO_PALAVRA + " FROM "
                + TABELA + " ORDER BY " + CAMPO_PALAVRA + " ASC";
        return sqLiteDatabase.rawQuery(query, null);
}
```

## Criação do Layout Principal
Selecione o arquivo de layout com o prefixo `content_suaactivity.xml` e crie o layout conforme a imagem abaixo e sua respectiva árvore de componentes.

Layout | Component Tree
-- | --
![](images/img04.png)  |  ![](images/img05.png)



## Adapter
Toda Lista precisa de um adapter, para esta lista em especial não iremos utilizar Layouts padrões do Android. Vamos utilizar um layout próprio, sendo um layout próprio, o Android não está preparado para ele, então precisamos criar uma Classe que fará adaptação de uma fonte de dados para os itens de nossa lista.
Abaixo seque o layout de um item de nossa lista.

<img src="images/img06.png" height="50px">

1. Crie o pacote adapter e dentro dele uma classe Java, a classe `SQLiteAdapter.java`.
2. Vamos criar uma model class para facilitar o encapsulamento e manutenção dos dados. Crie o pacote `model` no pacote principal.
3. Crie a classe `Dicionario.java` dentro do pacote model e adicione o código abaixo.
```javascript
public class Dicionario implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String palavra;
    private String definicao;

    public Dicionario(int id, String palavra, String definicao) {
        this.id = id;
        this.palavra = palavra;
        this.definicao = definicao;
    }

    public Dicionario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPalavra() {
        return palavra;
    }

    public void setPalavra(String palavra) {
        this.palavra = palavra;
    }

    public String getDefinicao() {
        return definicao;
    }

    public void setDefinicao(String definicao) {
        this.definicao = definicao;
    }

    @Override
    public String toString() {
        return "Dicionario{" +
                "id=" + id +
                ", palavra='" + palavra + '\'' +
                ", definicao='" + definicao + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dicionario)) return false;
        Dicionario that = (Dicionario) o;
        return id == that.id &&
                Objects.equals(palavra, that.palavra) &&
                Objects.equals(definicao, that.definicao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, palavra, definicao);
    }
}
```
4. Durante a aula discutiremos sobre este tipo de Adapters Customizados. Por hora, inclua o código abaixo na classe `SQLiteAdapter.java`.

```javascript
public class SQLiteAdapter extends BaseAdapter {
    private Context mContext;
    private List<Dicionario> dicionarioList;

    public SQLiteAdapter(Context context, List<Dicionario> dicionarioList) {
        this.mContext = context;
        this.dicionarioList = dicionarioList;
    }

    @Override
    public int getCount() {
        return dicionarioList.size();
    }

    @Override
    public Object getItem(int position) {
        return dicionarioList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Integer.valueOf(dicionarioList.get(position).getId());
    }

    public View getView(final int pos, View child, final ViewGroup parent) {
        Holder mHolder;
        if (child == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = layoutInflater.inflate(R.layout.cell_layout, null);
            mHolder = new Holder();
            mHolder.textViewId = child.findViewById(R.id.textViewId);
            mHolder.textViewPalavra = child.findViewById(R.id.textViewPalavra;
            mHolder.textViewDefinicao = child.findViewById(R.id.textViewDefinicao);
            child.setTag(mHolder);
        } else {
            mHolder = (Holder) child.getTag();
        }
        mHolder.textViewId.setText(String.valueOf(dicionarioList.get(pos).getId()));
        mHolder.textViewPalavra.setText(dicionarioList.get(pos).getPalavra());
        mHolder.textViewDefinicao.setText(dicionarioList.get(pos).getDefinicao());
        return child;
    }

    public class Holder {
        TextView textViewId;
        TextView textViewPalavra;
        TextView textViewDefinicao;
    }
  }
```


5. Criação da Custom View que receberá o binding de nosso Adapter. Crie o arquivo `res/layout/cell_layout.xml`  e adicione o xml abaixo, você também pode monta-lo pela paleta se ja estiver confortável com a confecção de layouts.
```javascript
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="10dp"
    android:orientation="vertical"
    android:padding="5dp">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:orientation="horizontal">

        <TextView
            android:id="@+id/textViewWord"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="Palavra"
            android:textSize="24sp"
            android:textStyle="bold" />

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_weight="4"
            android:orientation="vertical">

            <TextView
                android:id="@+id/textView"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Código"
                android:textAlignment="center" />

            <TextView
                android:id="@+id/textViewId"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="00"
                android:textAlignment="center" />
        </LinearLayout>

    </LinearLayout>

    <TextView
        android:id="@+id/textViewDefinition"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Definição" />
</LinearLayout>
```
## Código de interação com o Usuário
Agora iremos finalizar a aplicação de exemplo realizando a interação com o nosso `Helper` e nossa Activity.
1. Abaixo da declaração da Activity, insira a declaração dos objetos globais.
```javascript
... extends AppCompat{
  EditText mEditTextPalavra;
   EditText mEditTextDefinicao;
   MySQLiteOpenHelper mDB;
   ListView mListView;
   List<Dicionario> mDataSource = new ArrayList<>();
   ... resto do código
 }
```
2. Crie a função que irá exibir os dados armazenados no SQLite realizando binding através de nosso Adapter.
```javascript
public void exibirDadosEmLista(){
        mDataSource.clear();

        SQLiteDatabase sqLiteDatabase = mDB.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM dicionario",null);

        if (cursor.moveToFirst()){
            do {
                Dicionario dicionario = new Dicionario();
                dicionario.setId(cursor.getInt(cursor.getColumnIndex("_id")));
                dicionario.setPalavra(cursor.getString(cursor.getColumnIndex("palavra")));
                dicionario.setDefinicao(cursor.getString(cursor.getColumnIndex("definicao")));
                mDataSource.add(dicionario);
            }while (cursor.moveToNext());
        }

        SQLiteAdapter sqLiteAdapter =
                new SQLiteAdapter(MainActivity.this, mDataSource);
        sqLiteAdapter.notifyDataSetChanged();
        mListView.setAdapter(sqLiteAdapter);
        cursor.close();
}
```
3. No método `onCreate` inicialize os objetos, as caixas de texto, a lista e principalmente no `Helper` SQLite e faça a chamada ao método para preencher o `ListView`. Adicione o código abaixo.
```javascript
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mEditTextPalavra = findViewById(R.id.textInputPalavra);
        mEditTextDefinicao = findViewById(R.id.textInputSignificado);
        mListView = findViewById(R.id.listViewDicionario);

        mDB = new MySQLiteOpenHelper(this);

        exibirDadosEmLista();

        ...
      }
}
```
4. Crie o método para gravar um registro no banco a partir do `Fab Button`.
```javascript
private void saveRecord() {
        String palavra = mEditTextPalavra.getText().toString();
        String definicao = mEditTextDefinicao.getText().toString();
        mDB.salvarRegistro(palavra, definicao);
        mEditTextPalavra.setText("");
        mEditTextDefinicao.setText("");
        exibirDadosEmLista();
    }
```
5. Adicione a chamada dentro do listner do `Fab Button`. Dentro do método `onCreate`.
```javascript
FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            saveRecord();
            Snackbar.make(view, "Registro adicionado com sucesso!", Snackbar.LENGTH_LONG).show();
        }
    });
```
6. Para finalizarmos, vamos adicionar algumas ações nos itens da `ListView`; adicione o código abaixo no método `onCreate`.
```javascript
mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             Log.d("Position/ID: ",position + " - "+id);
             Toast.makeText(MainActivity.this,
                     mDB.buscarDefinicaoPalavra(id).toString(),
                     Toast.LENGTH_SHORT).show();
         } });

        mListView.setOnItemLongClickListener(new
             AdapterView.OnItemLongClickListener() {
                 @Override
                 public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                     Toast.makeText(MainActivity.this,
                             "Records deleted = " + mDB.apagarRegistro(id),Toast.LENGTH_SHORT).show();
                     exibirDadosEmLista();
                     return true;
                 }
             });
```
